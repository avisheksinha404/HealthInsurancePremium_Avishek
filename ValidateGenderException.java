package com.exceptions;

public class ValidateGenderException extends Exception {

	public ValidateGenderException(String msg){
		super(msg);
		System.out.println("Please correct gender");
		System.exit(0);
	}
}
