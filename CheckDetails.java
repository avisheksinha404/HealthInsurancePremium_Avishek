package com.business;

import com.exceptions.*;

public class CheckDetails {
	
	//to check a valid age
	public void validateAge(int age) {
		if(age<=0)
			try {
				throw new ValidateAgeException("not valid");
			} catch (ValidateAgeException e) {
				e.printStackTrace();
			}
	}
	//to check valid names and gender
	public void validateParameters(String name,String gender) {
		if(name.equals("")) {
			try {
				throw new ValidateNameException("not valid");
			} catch (ValidateNameException e) {
				e.printStackTrace();
			}
		}
		
		if(gender.equals("")) {
			try {
				throw new ValidateGenderException("not valid");
			} catch (ValidateGenderException e) {
				e.printStackTrace();
			}
	}
	
	}	

}
