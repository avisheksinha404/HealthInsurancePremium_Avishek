package com.exceptions;

public class ValidateNameException extends Exception{

	public ValidateNameException(String msg){
		super(msg);
		System.out.println("Please correct name");
		System.exit(0);
	}
}
