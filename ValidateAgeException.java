package com.exceptions;

public class ValidateAgeException extends Exception {
	
	public ValidateAgeException(String msg){
		super(msg);
		System.out.println("Please correct age");
		System.exit(0);
	}

}
