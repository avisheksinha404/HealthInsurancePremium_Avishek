//PremiumCalc.java
package com.business;

import javax.xml.bind.ValidationException;

public class PremiumCalc {
	
	//This method returns the changes the base premium amount based on age 
	public double getPremOnAge(int age) {
		double base=getBasePremium();
			
		if(age<18)
			return base;
			
		//checks as per condition if age is greater than 18 and less than 40 then increases base amount by 10 percent
		if(age>=18 && age <40) {
			return (base+0.1*base);
		}
		//returns base premium for person aged more than 40 years
		if(age>40) {
			base=base+0.2*base;
			int count=age-40;
			//increases base amount by 20 percent after every 5 years
			while(count>1){
				base=base+0.2*base;
				count=count/5;
			}
					
			return base;
		}
		//returns zero if condition doesnt meet
		return 0;
	}
	//return base amount 
	public double getBasePremium() {
		return 5000;
	}
	
	//increase base amount based on the gender condition
	public double getPremOnGender(String gender) {
		double base=getBasePremium();
		if(gender.equalsIgnoreCase("MALE"))
			return base+0.02*base;
		else
			return base;
	}
	
	//returns Premium amount based on Health Conditions
	public double getPremOnHealth(boolean hypertension,boolean bloodPressure,boolean bloodSugar,boolean overweight) {
		
		double base=getBasePremium();
		if(hypertension)
			base=base+0.01*base;
		if(bloodPressure)
			base=base+0.01*base;
		if(bloodSugar)
			base=base+0.01*base;
		if(overweight)
			base=base+0.01*base;
		return base;
			
	}
	//returns Premium amount based on Habits
	public double getPremOnHabits(boolean smoking,boolean alcohal,boolean excercise,boolean drugs) {
		
		double base=getBasePremium();
		if(excercise)
			base=base-0.03*base;
		if(smoking)
			base=base-0.03*base;
		if(alcohal)
			base=base-0.03*base;
		if(drugs)
			base=base-0.03*base;
		return base;
	}

}
