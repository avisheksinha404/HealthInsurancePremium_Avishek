//ClientApp.java
package com.client;

import java.util.Scanner;

import com.business.CheckDetails;
import com.business.PremiumCalc;
import com.model.MemberDetails;

public class ClientApp {

	public static void main(String[] args) {
		MemberDetails md=new MemberDetails();
		Scanner sc=new Scanner(System.in);
		System.out.print("\nName: ");
		//setting name
		md.setName(sc.next());
		System.out.print("\nGender: ");
		//setting gender
		md.setGender(sc.next());
		System.out.println(md.getGender()+"--");
		System.out.print("\nAge: ");
		//setting age
		md.setAge(sc.nextInt());
		
		CheckDetails ck=new CheckDetails();
		//validating age
		ck.validateAge(md.getAge());
		//validating name and gender
		ck.validateParameters(md.getName(),md.getGender());
		
		System.out.print("\nCurrent Health: ");
		//setting health details
		System.out.print("\nHypertension: ");
		String HT=sc.next();
		//System.out.println(md.health);
		if(HT.equalsIgnoreCase("No"))
			md.setHypertension(false);
		else
			md.setHypertension(true);
		
		System.out.print("\nBlood pressure: ");
		String BP=sc.next();
		if(BP.equalsIgnoreCase("No"))
			md.setBloodPressure(false);
		else
			md.setBloodPressure(true);
		
		System.out.print("\nBlood Sugar: ");
		String BS=sc.next();
		if(BS.equalsIgnoreCase("No"))
			md.setBloodSugar(false);
		else
			md.setBloodSugar(true);
		
		System.out.print("\nOverweight: ");
		String OW=sc.next();
		if(OW.equalsIgnoreCase("No"))
			md.setOverweight(false);
		else
			md.setOverweight(true);
		//setting habbits details
		System.out.print("\nHabits: ");
		System.out.print("\nSmoking: ");
		String SW=sc.next();
		if(SW.equalsIgnoreCase("No"))
			md.setSmoking(false);
		else
			md.setSmoking(true);
		
		System.out.print("\nAlcohal: ");
		String AL=sc.next();
		if(AL.equalsIgnoreCase("No"))
			md.setAlcohal(false);
		else
			md.setAlcohal(true);
		
		System.out.print("\nExcercise: ");
		String EX=sc.next();
		if(EX.equalsIgnoreCase("No"))
			md.setExcercise(false);
		else
			md.setExcercise(true);
		
		System.out.print("\nDrugs: ");
		String DG=sc.next();
		if(DG.equalsIgnoreCase("No"))
			md.setDrugs(false);
		else
			md.setDrugs(true);
		
		PremiumCalc calc=new PremiumCalc();
		//calculating premium based on age
		double p1=calc.getPremOnAge(md.getAge());
		//calculating premium based on gender
		double p2=calc.getPremOnGender(md.getGender());
		//calculating premium based on health conditions
		double p3=calc.getPremOnHealth(md.isHypertension(),md.isBloodPressure(),md.isBloodSugar(),md.isOverweight());
		//calculating premium based on habits
		double p4=calc.getPremOnHabits(md.isSmoking(), md.isAlcohal(), md.isExcercise(), md.isDrugs());
		//summing up all the premiums amount
		double totalPrem=p1+p2+p3+p4;
		
		//Printing the result
		System.out.println("Health Insurance Premium for Mr. "+md.getName()+": Rs."+totalPrem);
		
		
	}

}
